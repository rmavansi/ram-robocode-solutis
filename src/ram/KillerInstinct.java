package ram;
import robocode.*;
import java.awt.Color;

public class KillerInstinct extends AdvancedRobot {
	Color color = new Color(74, 12, 107); // Purple
	
    String behavior = "Control";
    
	int bulletCounter = 0;
	int positionCounter = 0;
	
	double lastPosition = 0;
	
	public void run() {
		// Set purple color
		setColors(color, color, color);
		setBulletColor(color);
		setScanColor(color);
		
		while(true) {			
			if(behavior=="Control" || behavior=="Aggro") {
				turnRadarRight(360);
			} else {
				setTurnRight(500);
				setMaxVelocity(5);
				ahead(500);
			}
		}
	}
	
	public void onScannedRobot(ScannedRobotEvent scannedRobotEvent) {
		if(lastPosition==scannedRobotEvent.getDistance()) {
			positionCounter++;
			if(positionCounter > 10) {
				behavior = "Spin";
				setBack(100);
			}
		} else {
			lastPosition = scannedRobotEvent.getDistance();
			positionCounter = 0;
		}
		
		if(behavior=="Control") {
			turnRight(scannedRobotEvent.getBearing());
	
			if(scannedRobotEvent.getDistance() < 100) {
				setBack(50);
			}
			if(scannedRobotEvent.getDistance() > 250) {
				setAhead(25);
				checkAndFire();
			} else if (scannedRobotEvent.getDistance() >= 150) {
				setAhead(25);
				fire(2);
			} else {
				checkAndFire();
			}
		} else if(behavior=="Aggro") {
            turnRight(scannedRobotEvent.getBearing());
            ahead(scannedRobotEvent.getDistance() + 5);
		} else {
			checkAndFire();
		}	
	}

	public void onHitWall(HitWallEvent hitWallEvent) {
		setBack(20);
		setTurnLeft(20);
	}	

	public void onHitRobot(HitRobotEvent hitRobotEvent) {
		turnRight(hitRobotEvent.getBearing()); 
        checkAndFire();
	}
	
	public void onBulletHitBullet(BulletHitBulletEvent bulletHitBulletEvent) {
	    bulletCounter = 0;
	}
	
	public void onBulletMissed(BulletMissedEvent bulletMissedEvent) {
		bulletCounter++;
		// After 15 misses change the behavior to aggro
		if(bulletCounter > 15) {
			behavior = "Aggro";
		}
	}

	// Victory dance o/
	public void onWin(WinEvent winEvent) {
		turnRight(36000);
    }
    
    public void checkAndFire() {
        if (getGunHeat() == 0) {
			fire(3);
		}
    }
}
