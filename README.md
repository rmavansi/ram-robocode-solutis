
# Killer Instinct

<img src=".gitlab/KillerInstinct.png" >

## :bookmark_tabs: Índice
- <a href="#robocode">Robocode</a>
- <a href="#projeto">Projeto</a>
- <a href="#author">Autor</a>
- <a href="#license">Licença</a>

## :robot: Robocode  <a name="robocode"></a>

[Robocode](https://robocode.sourceforge.io/) é um game de programação, utilizado para aprender programação de uma maneira divertida. O jogo é baseado em batalhas de robôs em tempo real, feitos em .NET ou Java.

## :blue_book: Projeto <a name="projeto"></a>

Robô chamado _Killer Instinct_ feito para batalhar na seletiva [Talent Sprint](https://www.solutis.com.br/talentsprint/) da [Solutis](https://solutis.com.br/).

### Linguagem de programação

- [Java](https://www.oracle.com/java/technologies/java-se-glance.html)

### Comportamento

_Killer Instinct_ tenta mudar seu comportamento de acordo com o comportamento do robô adversário. Ele começa girando o radar em 360° a todo momento procurando algum robô. Quando encontra alguém, ele avança pra cima do robô e atira, mas mantém uma certa distância. Caso o adversário se afaste, ele vai pra cima, e caso o adversário avance, ele recua. Porém, se for um adversário estático, _Killer Instinct_ recua e muda o comportamento para andar em círculos e atirar sempre que o radar encontrar um inimigo. Caso erre vários tiros seguidos (reseta quando acerta um tiro em outro tiro), _Killer Instinct_ toma uma postura agressiva, e vai pra cima do adversário.

```mermaid
stateDiagram
    [*] --> Controle
    Controle --> Giro
    Agro --> Giro
    Controle --> Agro
    Giro --> Agro
```

### Pontos fortes

- _Killer Instinct_ foi relativamente bem em um "battle royale" (32 robôs lutando ao mesmo tempo) com os robôs que eu tinha disponível;
- Na batalha com 5 robôs também teve um bom desempenho;
- Contra robôs com baixa mobilidade;

### Pontos fracos

- O _Killer Instinct_ é fraco contra robôs que tentam desviar dos tiros no 1vs1;
- Também é fraco com robôs com alta mobilidade;

### O que mais gostei de ter feito/aprendido

- Aprendi que é muito importante ler um pouco os códigos de exemplo e a api antes de já sair programando; :grin:
- Programar máquinas de combate é bem divertido;

## :man: Autor <a name="author"></a>

Renan Avansi Marques - <rmavansi@gmail.com>

## :memo: Licença <a name="license"></a>

Esse projeto está sob a licença MIT. Veja o arquivo [LICENSE](LICENSE) para mais detalhes.
